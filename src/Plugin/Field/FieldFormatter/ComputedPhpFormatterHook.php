<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\computed_field_php_formatter\Plugin\Field\FieldFormatter\ComputedPhpFormatterPhp as Base;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'PHP Hook' formatter for computed fields.
 * @FieldFormatter(
 *   id = "computed_php_hook",
 *   label = @Translation("Computed PHP (with hook function)"),
 *   field_types = {
 *     "computed_integer",
 *     "computed_decimal",
 *     "computed_float",
 *     "computed_string",
 *     "computed_string_long",
 *   }
 * )
 */
class ComputedPhpFormatterHook extends Base {

  /**
   * {@inheritdoc}
   */
  static function defaultSettings(): array {
    return [
        'unknown' => '?',
        'thousand_separator' => '',
        'decimal_separator' => '.',
        'scale' => 2,
        'prefix_suffix' => true,
      ] + parent::defaultSettings();
  }

  /**
   * @return boolean|null
   */
  protected function checkIntegerType() {
    $type = $this->fieldDefinition->getType();
    $items = ['computed_integer' => true, 'computed_decimal' => false, 'computed_float' => false];
    return $items[$type] ?? null;
  }

  /**
   * {@inheritdoc}
   */
  function settingsForm(array $form, FormStateInterface $form_state): array {
    $items = parent::settingsForm($form, $form_state);
    $items['php_code']['#disabled'] = $this->formatFunctionNameExists();
    $items['php_code']['#description'] .= t('
<p>
  <em><strong>WARNING:</strong> We strongly recommend that code be provided by a
  hook implementation in one of your custom modules, not here. This is far more
  secure than allowing code to be entered into this form from the Web UI. In
  addition, any code saved here will be stored in the database instead of your
  revision control system, which probably is not what you want. The hook
  implementation function signature should be
  <strong>%function($value_raw, $value_escaped, $item, $delta, $langcode)</strong>,
  and the desired value should be returned. If/when it exists, this form element
  will be greyed out.</em>
</p>    
    ', ['%function' => $this->getFormatFunctionName()]);

    $checked = $this->checkIntegerType();

    if (null !== $checked) {
      $items += $checked ? $this->integerSettingsForm($form, $form_state) : $this->decimalSettingsForm($form, $form_state);
    }

    return $items;
  }

  /**
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  protected function integerSettingsForm(array $form, FormStateInterface $form_state): array {
    $items = [];

    $options = [
      '' => t('- None -'),
      '.' => t('Decimal point'),
      ',' => t('Comma'),
      ' ' => t('Space'),
      chr(8201) => t('Thin space'),
      "'" => t('Apostrophe'),
    ];

    $items['unknown'] = [
      '#type' => 'textfield',
      '#title' => t('Unknown text'),
      '#required' => true,
      '#default_value' => $this->getSetting('unknown'),
    ];

    $items['thousand_separator'] = [
      '#type' => 'select',
      '#title' => t('Thousand marker'),
      '#options' => $options,
      '#default_value' => $this->getSetting('thousand_separator'),
      '#weight' => 0,
    ];

    $items['prefix_suffix'] = [
      '#type' => 'checkbox',
      '#title' => t('Display prefix and suffix'),
      '#default_value' => $this->getSetting('prefix_suffix'),
      '#weight' => 10,
    ];

    return $items;
  }

  /**
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  protected function decimalSettingsForm(array $form, FormStateInterface $form_state): array {
    $items = $this->integerSettingsForm($form, $form_state);

    $items['decimal_separator'] = [
      '#type' => 'select',
      '#title' => t('Decimal marker'),
      '#options' => ['.' => t('Decimal point'), ',' => t('Comma')],
      '#default_value' => $this->getSetting('decimal_separator'),
      '#weight' => 5,
    ];

    $items['scale'] = [
      '#type' => 'number',
      '#title' => t('Scale', [], ['context' => 'decimal places']),
      '#min' => 0,
      '#max' => 10,
      '#default_value' => $this->getSetting('scale'),
      '#description' => t('The number of digits to the right of the decimal.'),
      '#weight' => 6,
    ];

    return $items;
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  function formatItem($value_raw, FieldItemInterface $field_item, $delta = 0, $langcode = null): string {
    $settings = $this->getSettings();
    $value_escaped = Html::escape($value_raw);
    $display_value = null;

    if ($this->formatFunctionNameExists()) {
      $compute_function = $this->getFormatFunctionName();
      $display_value = $compute_function($value_raw, $value_escaped, $field_item, $delta, $langcode);
    } else {
      eval($settings['php_code']);
    }

    return $this->wrapperFormat($field_item, $display_value);
  }

  /**
   * @param string       $number
   * @param boolean|null $checked
   *
   * @return string
   */
  protected function numberFormat($number, $checked): string {
    if (null !== $checked) {
      $number = $checked ? $this->integerFormat($number) : $this->decimalFormat($number);
    }

    return $number;
  }

  /**
   * @param string $number
   *
   * @return string
   */
  protected function decimalFormat($number): string {
    $items = $this->getSettings();
    $scale = intval($items['scale']);
    $decimal_separator = (0 === $scale) ? '' : $items['decimal_separator'];
    return number_format(floatval($number), $scale, $decimal_separator, $items['thousand_separator']);
    //    return Decimal::create($number, $scale)->format($scale, $decimal_separator, $items['thousand_separator']);
  }

  /**
   * @param string $number
   *
   * @return string
   */
  protected function integerFormat($number): string {
    $scale = 0;
    return number_format(floatval($number), $scale, '', $this->getSetting('thousand_separator'));
    //    return Decimal::create($number, $scale)->format($scale, '', $this->getSetting('thousand_separator'));
  }

  /**
   * @param FieldItemInterface $field_item
   * @param string             $display_value
   *
   * @return string
   */
  protected function wrapperFormat(FieldItemInterface $field_item, $display_value): string {
    $checked = $this->checkIntegerType();

    if (null === $checked) {
      return $display_value;
    }

    /** @var string $unknown */
    $unknown = $this->getSetting('unknown');

    if (false === is_numeric($display_value)) {
      return $unknown;
    }

    $settings = $this->getFieldSettings();
    $output = $this->numberFormat($display_value, $checked);

    // Account for prefix and suffix.
    if (('' !== $output) && ($this->getSetting('prefix_suffix'))) {
      $prefixes = isset($settings['prefix']) ? array_map([
        'Drupal\Core\Field\FieldFilteredMarkup',
        'create'
      ], explode('|', $settings['prefix'])) : [''];

      $suffixes = isset($settings['suffix']) ? array_map([
        'Drupal\Core\Field\FieldFilteredMarkup',
        'create'
      ], explode('|', $settings['suffix'])) : [''];

      $prefix = (count($prefixes) > 1) ? $this->formatPlural($display_value, $prefixes[0], $prefixes[1]) : $prefixes[0];
      $suffix = (count($suffixes) > 1) ? $this->formatPlural($display_value, $suffixes[0], $suffixes[1]) : $suffixes[0];
      $output = $prefix . $output . $suffix;
    }

    // Output the raw value in a content attribute if the text of the HTML
    // element differs from the raw value (for example when a prefix is used).
    if (isset($field_item->_attributes) && ($display_value != $output)) {
      $field_item->_attributes += ['content' => $display_value];
    }

    return ('' === $output) ? $unknown : $output;
  }

  /**
   * Fetches this field's format function name for implementing elsewhere.
   *
   * @return string
   *   The function name.
   */
  protected function getFormatFunctionName(): string {
    $field_name = $this->fieldDefinition->getName();
    return 'computed_field_' . $field_name . '_format';
  }

  /**
   * Determines if a format function exists for this field.
   *
   * @return bool
   */
  protected function formatFunctionNameExists(): bool {
    return function_exists($this->getFormatFunctionName());
  }

  /**
   * {@inheritdoc}
   */
  function settingsSummary(): array {
    $items = parent::settingsSummary();
    $checked = $this->checkIntegerType();

    if (null === $checked) {
      return $items;
    }

    $items[] = $this->numberFormat(1234.1234567890, $checked);

    if ($this->getSetting('prefix_suffix')) {
      $items[] = t('Display with prefix and suffix.');
    }

    return $items;
  }

}
