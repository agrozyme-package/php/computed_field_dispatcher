<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\ComputedField;

use Drupal\Core\Entity\EntityInterface;
use Drupal\hook_event_dispatcher\Event\EventInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ComputedFieldComputeEvent
 */
class ComputedFieldComputeEvent extends Event implements EventInterface {
  protected $entity;
  protected $delta;
  protected $fieldName;

  protected $value = '';

  /**
   * @param EntityInterface $entity
   * @param int             $delta
   * @param string          $fieldName
   */
  function __construct(EntityInterface $entity, int $delta, string $fieldName) {
    $this->entity = $entity;
    $this->delta = $delta;
    $this->fieldName = $fieldName;
  }

  /**
   * {@inheritdoc}
   */
  function getDispatcherType(): string {
    return 'hook_event_dispatcher.computed_field.' . $this->getEntityType() . '.' . $this->getFieldName() . '.compute';
  }

  /**
   * @return EntityInterface
   */
  function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * @return int
   */
  function getDelta(): int {
    return $this->delta;
  }

  /**
   * @return string
   */
  function getFieldName(): string {
    return $this->fieldName;
  }

  /**
   * @return mixed
   */
  function getValue() {
    return $this->value;
  }

  /**
   * @param mixed $value
   */
  function setValue($value) {
    $this->value = $value;
  }

  /**
   * @return string
   */
  function getEntityType(): string {
    return $this->getEntity()->getEntityTypeId();
  }

}
