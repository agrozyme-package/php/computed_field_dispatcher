<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\ComputedField;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\hook_event_dispatcher\Event\EventInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ComputedFieldFormatEvent
 */
class ComputedFieldFormatEvent extends Event implements EventInterface {
  protected $valueRaw;
  protected $fieldItem;
  protected $delta;
  protected $langcode;

  protected $value = '';

  /**
   * @param mixed              $valueRaw
   * @param FieldItemInterface $fieldItem
   * @param int                $delta
   * @param string             $langcode
   */
  function __construct($valueRaw, FieldItemInterface $fieldItem, int $delta, string $langcode) {
    $this->valueRaw = $valueRaw;
    $this->fieldItem = $fieldItem;
    $this->delta = $delta;
    $this->langcode = $langcode;
  }

  /**
   * {@inheritdoc}
   */
  function getDispatcherType(): string {
    return 'hook_event_dispatcher.computed_field.' . $this->getEntityType() . '.' . $this->getFieldName() . '.format';
  }

  /**
   * @return mixed
   */
  function getValueRaw() {
    return $this->valueRaw;
  }

  /**
   * @return FieldItemInterface
   */
  function getFieldItem(): FieldItemInterface {
    return $this->fieldItem;
  }

  /**
   * @return int
   */
  function getDelta(): int {
    return $this->delta;
  }

  /**
   * @return string
   */
  function getLangcode(): string {
    return $this->langcode;
  }

  /**
   * @return string
   */
  function getValue(): string {
    return $this->value;
  }

  /**
   * @param string $value
   */
  function setValue(string $value) {
    $this->value = $value;
  }

  /**
   * @return string
   */
  function getFieldName(): string {
    return $this->getFieldItem()->getFieldDefinition()->getName();
  }

  /**
   * @return string
   */
  function getEntityType(): string {
    return $this->getFieldItem()->getEntity()->getEntityTypeId();
  }

}
