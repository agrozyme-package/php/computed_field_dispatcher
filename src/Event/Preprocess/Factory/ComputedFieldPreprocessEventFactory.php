<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\Preprocess\Factory;

use Drupal\computed_field_dispatcher\Event\Preprocess\ComputedFieldPreprocessEvent;
use Drupal\computed_field_dispatcher\Event\Preprocess\Variables\ComputedFieldEventVariables;
use Drupal\hook_event_dispatcher\Event\Preprocess\AbstractPreprocessEvent;
use Drupal\hook_event_dispatcher\Event\Preprocess\Factory\PreprocessEventFactoryInterface;

/**
 * Class ComputedFieldPreprocessEventFactory
 */
class ComputedFieldPreprocessEventFactory implements PreprocessEventFactoryInterface {

  /**
   * {@inheritdoc}
   */
  function createEvent(array &$variables): AbstractPreprocessEvent {
    return new ComputedFieldPreprocessEvent(new ComputedFieldEventVariables($variables));
  }

  /**
   * {@inheritdoc}
   */
  function getEventHook(): string {
    return ComputedFieldPreprocessEvent::getHook();
  }
}
