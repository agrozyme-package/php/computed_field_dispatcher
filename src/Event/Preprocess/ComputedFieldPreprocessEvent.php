<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\Preprocess;

use Drupal\hook_event_dispatcher\Event\Preprocess\AbstractPreprocessEvent;

/**
 * Class ComputedFieldPreprocessEvent
 */
class ComputedFieldPreprocessEvent extends AbstractPreprocessEvent {

  /**
   * {@inheritdoc}
   */
  static function getHook(): string {
    return 'computed_field';
  }
}
