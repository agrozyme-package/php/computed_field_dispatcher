<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\Preprocess\Variables;

use Drupal\hook_event_dispatcher\Event\Preprocess\Variables\AbstractEventVariables;

/**
 * Class ComputedFieldEventVariables
 */
class ComputedFieldEventVariables extends AbstractEventVariables {

}
